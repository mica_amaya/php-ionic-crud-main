import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    public http: HttpClient
  ) { }

  addStudents(data) { 
    return this.http.post('http://localhost/php-ionic-crud-main/backend/create.php',data);
  }

  getStudents() {
    return this.http.get('http://localhost/php-ionic-crud-main/backend/getStudents.php');
  }

  
  deleteStudent(id) {
    return this.http.delete('http://localhost/php-ionic-crud-main/backend/delete.php?id='+id);
  }

  getStudent(id) {
    return this.http.get('http://localhost/php-ionic-crud-main/backend/getSingleStudent.php?id='+id);
  }

  updateStudent(id, data){
    return this.http.put('http://localhost/php-ionic-crud-main/backend/updateStudent.php?id='+id,data);    
  }


}